from flask import Flask, render_template, request
from googlesearch import search
import webbrowser

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/idk')
def googling():
    
    question = request.args['question']
    question = 'gate'+question
    for url in search(question, stop=5):
        print(url)
        if "gateoverflow" in url:
            print("yes")
            webbrowser.open(url, new=2)
            break
            
    return render_template('index.html')

@app.route('/test')
def test():
    return render_template('test.html')

if __name__ == "__main__":
    app.run(debug=True)